require 'resque_web'
require 'resque/scheduler/server'
Vmscheduler::Application.routes.draw do
  devise_for :users
  resources :jobs, only: [:index, :show]

  resources :scheduled_actions

  resources :user_providers


  resources :actions


  resources :vms

  root to: 'dashboard#index'

  mount Resque::Server.new, at: "/resque"

end
