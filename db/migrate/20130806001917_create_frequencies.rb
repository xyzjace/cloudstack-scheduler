class CreateFrequencies < ActiveRecord::Migration
  def change
    create_table :frequencies do |t|
      t.string :name
      t.string :cron

      t.timestamps
    end
  end
end
