class RenameScheduledToRecurringOnScheduledActions < ActiveRecord::Migration
  def change
    rename_column :scheduled_actions, :scheduled, :recurring
  end
end
