class AddScheduledToScheduledAction < ActiveRecord::Migration
  def change
    add_column :scheduled_actions, :scheduled, :boolean
  end
end
