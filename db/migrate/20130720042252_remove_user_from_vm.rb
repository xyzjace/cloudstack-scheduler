class RemoveUserFromVm < ActiveRecord::Migration
  def up
    remove_column :vms, :user_id
  end

  def down
    add_column :vms, :user_id, :references
  end
end
