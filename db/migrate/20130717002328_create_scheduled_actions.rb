class CreateScheduledActions < ActiveRecord::Migration
  def change
    create_table :scheduled_actions do |t|
      t.references :vm
      t.string :action
      t.references :user
      t.datetime :scheduled_time
      t.string :frequency

      t.timestamps
    end
    add_index :scheduled_actions, :vm_id
    add_index :scheduled_actions, :user_id
  end
end
