class CreateVms < ActiveRecord::Migration
  def change
    create_table :vms do |t|
      t.string :name
      t.string :uuid
      t.references :user
      t.text :api_key
      t.text :api_secret

      t.timestamps
    end
    add_index :vms, :user_id
  end
end
