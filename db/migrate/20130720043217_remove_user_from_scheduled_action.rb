class RemoveUserFromScheduledAction < ActiveRecord::Migration
  def up
    remove_column :scheduled_actions, :user_id
  end

  def down
    add_column :scheduled_actions, :user_id, :references
  end
end
