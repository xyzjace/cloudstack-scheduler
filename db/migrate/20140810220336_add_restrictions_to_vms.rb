class AddRestrictionsToVms < ActiveRecord::Migration
  def change
    change_column :vms, :name, :string, null: false
    change_column :vms, :uuid, :string, null: false
    change_column :vms, :api_key, :text, null: false
    change_column :vms, :api_secret, :text, null: false
  end
end
