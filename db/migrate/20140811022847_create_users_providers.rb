class CreateUsersProviders < ActiveRecord::Migration
  def change
    create_table :users_providers do |t|
      t.integer :user_id, null: false
      t.integer :provider_id, null: false
      t.string :api_key, null: false
      t.string :api_secret, null: false

      t.timestamps
    end
  end
end
