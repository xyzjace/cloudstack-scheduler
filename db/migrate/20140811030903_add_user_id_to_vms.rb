class AddUserIdToVms < ActiveRecord::Migration
  def change
    add_column :vms, :user_id, :integer, null: false
  end
end
