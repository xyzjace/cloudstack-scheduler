class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.references :scheduled_action
      t.string :cloudstack_job
      t.string :action_result

      t.timestamps
    end
    add_index :jobs, :scheduled_action_id
    add_index :jobs, :cloudstack_job
  end
end
