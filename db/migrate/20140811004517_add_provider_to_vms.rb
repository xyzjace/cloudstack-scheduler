class AddProviderToVms < ActiveRecord::Migration
  def change
    add_column :vms, :provider_id, :integer, null: false
  end
end
