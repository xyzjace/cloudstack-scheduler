require 'time'
require 'fog'

module VmScheduler
  @queue = :vm_schedule

  def self.perform(schedule_id)
    scheduled_action = load_scheduled_action(schedule_id)

    job = Job.create!(scheduled_action_id: scheduled_action.id,
                      cloudstack_job: nil,
                      action_result: ActionResult::Scheduled
    )

    load_provider(scheduled_action.vm)

    job.update(action_result: ActionResult::Running)

    result = @provider.send("#{scheduled_action.action.name}_vm".to_sym, scheduled_action.vm)

    if result['jobid']
      job.update(cloudstack_job: result['jobid'])
      p "Scheduled job #{job.inspect} for VM #{scheduled_action.vm.inspect}."
    else
      job.update(action_result: ActionResult::Failed)
      raise "Could not complete #{scheduled_action.inspect}. Cloudstack returned #{result.inspect}"
    end
    job.update(action_result: ActionResult::WaitingForResults)
  end

  def self.load_scheduled_action(id)
    scheduled_action = ScheduledAction.find(id)
    unless scheduled_action
      raise "Could not find ScheduledAction with id: #{id}"
    end
    scheduled_action
  end

  def self.load_provider(vm)
    @provider = BuildVmConnection.perform(vm.provider, vm.api_key, vm.api_secret)
  end

    #change actionresult to scheduled
    #after the call is made, change to running, or else fail
    #then schedule a 2 minute delayed task which checks status of the job
    #if the job is success, status changes to success
    #if the job is unfinished, schedule a final check for 5 minutes later
    #in the final check, change status to success or failure
    #you can queue all 3 jobs (extra processing if the job is successful)
    #or you can make the first delayed check call its own delayed check


end
