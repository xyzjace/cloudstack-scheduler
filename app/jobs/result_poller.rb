module ResultPoller
  @queue = :result_poller

  def self.perform
    # the nearest I can figure jobstatus == 1 is success
    #{"accountid"=>"75de9ff8-37c7-4959-bca9-e79614dd5407", "userid"=>"502b42fc-814a-4122-b3e3-adce1ea00335", "cmd"=>"com.cloud.api.commands.StartVMCmd", "jobstatus"=>1, "jobprocstatus"=>0, "jobresultcode"=>0, "jobresulttype"=>"object", "jobresult"=>{"virtualmachine"=>{"id"=>"c3ede3ab-da8b-4d30-ac2d-03a53786ac61", "name"=>"apitester-3815", "displayname"=>"apitester", "project"=>"NF000270-default", "domainid"=>"872a7f4a-4a06-470f-b1cf-98e1b84bd507", "domain"=>"NF000270", "created"=>"2013-08-07T17:00:28+1000", "state"=>"Running", "haenable"=>false, "zoneid"=>"72edbc6f-8d32-4e4e-9fae-2a9cfcd5f772", "zonename"=>"AU-Sydney-1", "templateid"=>"2943770f-51fc-47b2-b211-74344dc84cf8", "templatename"=>"Debian 7.0", "templatedisplaytext"=>"Debian 7.0", "passwordenabled"=>true, "serviceofferingid"=>"82896ebd-c681-4cec-aeff-fde20c63fa76", "serviceofferingname"=>"512MB-X1", "cpunumber"=>1, "cpuspeed"=>1100, "memory"=>512, "cpuused"=>"5.36%", "networkkbsread"=>1, "networkkbswrite"=>1, "guestosid"=>"133", "rootdeviceid"=>0, "rootdevicetype"=>"EXT", "securitygroup"=>[], "nic"=>[{"id"=>"5f838cf9-1873-47ac-b840-50779a54ea48", "networkid"=>"28fc7699-7b44-4922-815c-fdb23411f926", "netmask"=>"255.255.255.0", "gateway"=>"10.102.1.1", "ipaddress"=>"10.102.1.128", "traffictype"=>"Guest", "type"=>"Isolated", "isdefault"=>true, "macaddress"=>"02:00:75:10:00:04"}], "hypervisor"=>"XenServer", "publicipid"=>"48c671a7-086d-43be-9b60-3c66595b57cf", "publicip"=>"125.7.10.234", "tags"=>[]}}, "created"=>"2014-06-04T09:36:19+1000", "jobid"=>"d006aa64-b7e7-4917-ba23-562416527ea7"}
    Job.where("action_result = ?", "waiting_for_results").each do |job|
      provider = ProviderConnection::Ninefold.new(job.scheduled_action.vm.api_key, job.scheduled_action.vm.api_secret)
      result = provider.check_job_status(job)
      if result["jobstatus"].to_s == "1"
        job.update(action_result: ActionResult::Succeeded)
      end
    end
  end
end
