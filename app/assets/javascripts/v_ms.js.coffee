check_vm_count = ->
  if $("[id^=new_vm]").length < 1
    $("#no_more_vms").show()
    true
  else
    false

ready = ->

  if !check_vm_count()
    $("[id^=new_vm]").each (index) ->
      $(this).children(':input').prop('disabled', false)
      errorField = $('#error_' + index)
      $(this).on("ajax:success", (e, data, status, xhr) ->
        errorField.hide()
        $(this).children(':input').prop('disabled', true)
        $(this).remove()
        check_vm_count()
        return
      ).on "ajax:error", (e, xhr, status, error) ->
        $(this).show()
        $(this).children(':input').prop('disabled', false)
        errorField.text($.parseJSON(xhr.responseText))
        errorField.show()
        return
    return


$(document).ready(ready)
$(document).on('page:load', ready)
