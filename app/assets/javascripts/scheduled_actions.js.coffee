ready = ->
  $(".scheduled_action_frequency").hide()
  $("input[name=\"scheduled_action[frequency]\"]").prop "required", false

  $("#scheduled_action_recurring").click ->
    if $(this).prop("checked") is true
      $(".scheduled_action_frequency").show()
      $("input[name=\"scheduled_action[frequency]\"]").prop "required", true
    else
      $(".scheduled_action_frequency").hide()
      $("input[name=\"scheduled_action[frequency]\"]").prop "required", false
    return

$(document).ready(ready)
$(document).on('page:load', ready)
