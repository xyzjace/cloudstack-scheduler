class DashboardController < ApplicationController

  def index
    @vms = Vm.all
    @scheduled_actions = ScheduledAction.all
    @jobs = Job.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @scheduled_actions }
    end
  end

  private

end
