class UserProvidersController < ApplicationController
  before_action :set_user_provider, only: [:show, :edit, :update, :destroy]

  # GET /user_providers
  def index
    @user_providers = UserProvider.all
  end

  # GET /user_providers/1
  def show
  end

  # GET /user_providers/new
  def new
    @user_provider = UserProvider.new
  end

  # GET /user_providers/1/edit
  def edit
  end

  # POST /user_providers
  def create
    @user_provider = UserProvider.new(user_loaded_params)

    if @user_provider.save
      redirect_to @user_provider, notice: 'User provider was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /user_providers/1
  def update
    if @user_provider.update(user_loaded_params)
      redirect_to @user_provider, notice: 'User provider was successfully updated.'
    else
      render action: 'edit'
    end
  end

  # DELETE /user_providers/1
  def destroy
    @user_provider.destroy
    redirect_to user_providers_url, notice: 'User provider was successfully destroyed.'
  end

  private

  def user_loaded_params
    user_provider_params.merge(:user_id => current_user.id)
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_user_provider
    @user_provider = UserProvider.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def user_provider_params
    params.require(:user_provider).permit(:provider_id, :api_key, :api_secret)
  end
end
