class ScheduledActionsController < ApplicationController
  # GET /scheduled_actions
  # GET /scheduled_actions.json
  def index
    @scheduled_actions = ScheduledAction.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @scheduled_actions }
    end
  end

  # GET /scheduled_actions/1
  # GET /scheduled_actions/1.json
  def show
    @scheduled_action = ScheduledAction.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @scheduled_action }
    end
  end

  # GET /scheduled_actions/new
  # GET /scheduled_actions/new.json
  def new
    @scheduled_action = ScheduledAction.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @scheduled_action }
    end
  end

  # GET /scheduled_actions/1/edit
  def edit
    @scheduled_action = ScheduledAction.find(params[:id])
  end

  # POST /scheduled_actions
  # POST /scheduled_actions.json
  def create
    @scheduled_action = ScheduledAction.new(scheduled_action_params)

    respond_to do |format|
      if @scheduled_action.save
        format.html { redirect_to @scheduled_action, notice: 'Scheduled action was successfully created.' }
        format.json { render json: @scheduled_action, status: :created, location: @scheduled_action }
      else
        format.html { render action: "new" }
        format.json { render json: @scheduled_action.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /scheduled_actions/1
  # PUT /scheduled_actions/1.json
  def update
    @scheduled_action = ScheduledAction.find(params[:id])

    respond_to do |format|
      if @scheduled_action.update_attributes(params[:scheduled_action])
        format.html { redirect_to @scheduled_action, notice: 'Scheduled action was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @scheduled_action.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /scheduled_actions/1
  # DELETE /scheduled_actions/1.json
  def destroy
    @scheduled_action = ScheduledAction.find(params[:id])
    @scheduled_action.destroy

    respond_to do |format|
      format.html { redirect_to scheduled_actions_url }
      format.json { head :no_content }
    end
  end

  private

  def scheduled_action_params
    params.require(:scheduled_action).permit(:vm_id, :action, :scheduled_time, :recurring, :frequency)
  end

  def load_vm
    @vm = Vm.find(params[:vm_id])
  end

end
