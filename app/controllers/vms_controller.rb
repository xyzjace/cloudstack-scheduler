class VmsController < ApplicationController
  before_action :load_possible_new_vms, only: [:new]
  # GET /vms
  # GET /vms.json
  def index
    @vms = Vm.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @vms }
    end
  end

  # GET /vms/1
  # GET /vms/1.json
  def show
    @vm = Vm.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @vm }
    end
  end

  # GET /vms/new
  # GET /vms/new.json
  def new
    @vm = Vm.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @vm }
    end
  end

  # GET /vms/1/edit
  def edit
    @vm = Vm.find(params[:id])
  end

  # POST /vms
  # POST /vms.json
  def create
    @vm = Vm.new(user_loaded_params)

    if @vm.save
      render json: @vm
    else
      render json: @vm.errors.full_messages, status: :unprocessable_entity
    end
  end

  # PUT /vms/1
  # PUT /vms/1.json
  def update
    @vm = Vm.find(params[:id])

    respond_to do |format|
      if @vm.update_attributes(user_loaded_params)
        format.html { redirect_to @vm, notice: 'Vm was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @vm.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /vms/1
  # DELETE /vms/1.json
  def destroy
    @vm = Vm.find(params[:id])
    @vm.destroy

    respond_to do |format|
      format.html { redirect_to vms_url }
      format.json { head :no_content }
    end
  end

  private

  def load_possible_new_vms
    @vms = Vm.where(user: current_user)
    load_user_provider
    if @user_provider
      @vm_list = ListVms.perform(current_user, @user_provider.provider).reject { |vm| @vms.map(&:uuid).include?(vm['uuid']) }
    else
      @vm_list = []
    end
  end

  def load_user_provider
    if params[:provider_id]
      @user_provider = UserProvider.where(user: current_user, id: params[:provider_id]).first
    else
      @user_provider = UserProvider.where(user: current_user).first
    end
  end

  def user_loaded_params
    vm_params.merge(:user_id => current_user.id)
  end

  def vm_params
    params.require(:vm).permit(:name, :uuid, :api_key, :api_secret, :provider_id)
  end
end
