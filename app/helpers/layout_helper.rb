module LayoutHelper

  def link_class(path_list)
    Array.wrap(path_list).any? {|path| request.url.include?(path) } ? 'active' : nil
  end

end