class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :users_providers, class_name: 'UsersProviders'
  has_many :providers, through: :users_providers, class_name: 'Provider'
  has_many :vms, class_name: 'Vm'
end
