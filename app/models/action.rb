class Action < ClassyEnum::Base
  def name
    self.class.name.demodulize.downcase
  end
end

class Action::Start < Action
end

class Action::Stop < Action
end

class Action::Restart < Action
end
