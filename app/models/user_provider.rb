class UserProvider < ActiveRecord::Base
  self.table_name = 'users_providers'

  belongs_to :user, class_name: 'User'
  belongs_to :provider, class_name: 'Provider'

  validates_presence_of :user, :provider, :api_key, :api_secret
end
