class ProviderConnection::Ninefold

  def initialize(api_key, api_secret)
    @api_key = api_key
    @api_secret = api_secret
  end

  def start_vm(vm)
    connection.start_virtual_machine(id: vm.uuid)
  end

  def stop_vm(vm)
    connection.stop_virtual_machine(id: vm.uuid)
  end

  def restart_vm(vm)
    connection.reboot_virtual_machine(id: vm.uuid)
  end

  def check_job_status(job)
    connection.query_async_job_result({'jobid' => job.cloudstack_job})
  end

  def check_vm_status(vm)
    connection.list_virtual_machines({'id' => vm.uuid})['listvirtualmachinesresponse']['virtualmachine'].first.try(:[], 'state')
  end

  def list_vms(options = {})
    connection.list_virtual_machines({'projectid' => options['projectid']})["listvirtualmachinesresponse"]
  end

  def list_projects
    connection.list_projects["listprojectsresponse"]
  end

  private

  def connection
    @connection ||= Fog::Compute::Cloudstack.new(cloudstack_api_key: @api_key,
                                                 cloudstack_secret_access_key: @api_secret,
                                                 cloudstack_host: host,
                                                 cloudstack_path: path
    )
  end

  def provider
    'ninefold'
  end

  def host
    'api.ninefold.com'
  end

  def path
    '/compute/v2.0'
  end

  def account_id
    connection.list_accounts.first['id']
  end

  def domain_id
    connection.list_accounts.first['domainid']
  end

end
