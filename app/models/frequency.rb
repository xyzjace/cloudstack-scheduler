class Frequency < ClassyEnum::Base
  attr_reader :min, :hour

  def min=(minute)
    if minute.between >=0 && minute <= 59
      min = minute
    else
      raise "Minute must be between 0 and 59"
    end

  end

  def hour=(hr)
    if hr.between >= 0 && hr <= 23
      hour = hr
    else
      raise "Hour must be between 0 and 23"
    end
  end

end

class Frequency::EveryMinute < Frequency
  def cron_string(options={})
    "* * * * *"
  end
end

class Frequency::EveryHour < Frequency
  def cron_string(options={})
    min = options[:minute] || 1
    "#{min} * * * *"
  end
end

class Frequency::EveryDay < Frequency
  def cron_string(options={})
    min = options[:minute] || 1
    hour = options[:hour] || 1
    "#{min} #{hour} * * *"
  end
end

class Frequency::EveryMonth < Frequency
  def cron_string(options={})
    min = options[:minute] || 1
    hour = options[:hour] || 1
    day_of_month = options[:day_of_month] || 1
    "#{min} #{hour} #{day_of_month} * *"
  end
end

class Frequency::EveryMonday < Frequency
  def cron_string(options={})
    min = options[:minute] || 1
    hour = options[:hour] || 1
    "#{min} #{hour} * * 1"
  end
end

class Frequency::EveryTuesday < Frequency
  def cron_string(options={})
    min = options[:minute] || 1
    hour = options[:hour] || 1
    "#{min} #{hour} * * 2"
  end
end

class Frequency::EveryWednesday < Frequency
  def cron_string(options={})
    min = options[:minute] || 1
    hour = options[:hour] || 1
    "#{min} #{hour} * * 3"
  end
end

class Frequency::EveryThursday < Frequency
  def cron_string(options={})
    min = options[:minute] || 1
    hour = options[:hour] || 1
    "#{min} #{hour} * * 4"
  end
end

class Frequency::EveryFriday < Frequency
  def cron_string(options={})
    min = options[:minute] || 1
    hour = options[:hour] || 1
    "#{min} #{hour} * * 5"
  end
end

class Frequency::EverySaturday < Frequency
  def cron_string(options={})
    min = options[:minute] || 1
    hour = options[:hour] || 1
    "#{min} #{hour} * * 6"
  end
end

class Frequency::EverySunday < Frequency
  def cron_string(options={})
    min = options[:minute] || 1
    hour = options[:hour] || 1
    "#{min} #{hour} * * 0"
  end
end

class Frequency::EveryWeekday < Frequency
  def cron_string(minute=1, hour=1)
    min = options[:minute] || 1
    hour = options[:hour] || 1
    "#{min} #{hour} * * 1,2,3,4,5"
  end
end

