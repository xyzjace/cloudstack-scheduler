class Vm < ActiveRecord::Base

  has_many :scheduled_actions
  has_many :jobs, through: :scheduled_actions
  belongs_to :provider, class_name: 'Provider' # should really be user_provider. we can get the provider from the connection
  belongs_to :user, class_name: 'User'
  validates_presence_of :name, :uuid, :api_key, :api_secret, :provider, :user

  def to_s
    name
  end

  def state
    BuildVmConnection.perform(provider, api_key, api_secret).check_vm_status(self) || 'Unavailable'
  end

end
