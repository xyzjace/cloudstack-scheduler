class Job < ActiveRecord::Base

  classy_enum_attr :action_result
  belongs_to :scheduled_action
  #TODO:
  #relates to schedule, has a job_id (from cloudstack api), has a failure/success/running/scheduled
  #job id can be null prior to running
  def to_s
    cloudstack_job
  end
end
