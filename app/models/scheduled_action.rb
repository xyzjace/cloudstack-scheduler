class ScheduledAction < ActiveRecord::Base
  belongs_to :vm
  classy_enum_attr :action
  classy_enum_attr :frequency, allow_blank: true
  after_commit :schedule
  before_destroy :remove_from_schedule
  has_one :job

  def to_s
    "#{action} #{vm} #{scheduled_time}"
  end

  def schedule_name
    "#{action}#{vm}#{scheduled_time.to_i}"
  end

  def schedule
    if recurring and frequency.present?
      add_to_dynamic_schedule
    else
      Resque.enqueue_in_with_queue("vm_schedule", queue_in_seconds, "VmScheduler", id)
    end
  end

  def queue_in_seconds
    sec_from_now = (self.scheduled_time - DateTime.now).abs.round
    if sec_from_now < 0
      sec_from_now = 0
    end
    sec_from_now
  end

  def remove_from_schedule
    if recurring && frequency.present?
      Resque.remove_schedule(schedule_name)
    end
  end

  def add_to_dynamic_schedule
    Resque.set_schedule(schedule_name, {
      :cron => cron_string,
      :class => "VmScheduler",
      :queue => "vm_schedule",
      :args => id,
      :persist => true
    })
  end

  private

  def cron_string
    if recurring && frequency.present?
      minute = scheduled_time.min
      hour = scheduled_time.hour
      month = scheduled_time.month
      dom = scheduled_time.day
      cron_hash = {:minute => minute, :hour => hour, :day_of_month => dom}
      frequency.cron_string(cron_hash)
    else
      nil
    end
  end



end
