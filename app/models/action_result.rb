class ActionResult < ClassyEnum::Base
  def self.action
    self.name.demodulize.downcase
  end
end

class ActionResult::Scheduled < ActionResult
end

class ActionResult::Running < ActionResult
end

class ActionResult::WaitingForResults < ActionResult
end

class ActionResult::Succeeded < ActionResult
end

class ActionResult::Failed < ActionResult
end
