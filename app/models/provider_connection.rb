class ProviderConnection
  def start_vm(vm)
    raise "Must be implemented in the child class"
  end

  def stop_vm(vm)
    raise "Must be implemented in the child class"
  end

  def restart_vm(vm)
    raise "Must be implemented in the child class"
  end

  def check_job_status(job)
    raise "Must be implemented in the child class"
  end

  def check_vm_status(vm)
    raise "Must be implemented in the child class"
  end

  def list_vms(options = {})
    raise "Must be implemented in the child class"
  end

  def list_projects
    raise "Must be implemented in the child class"
  end
end
