class BuildVmConnection
  def self.perform(provider, api_key, api_secret)
    "ProviderConnection::#{provider.name}".constantize.new(api_key, api_secret)
  end
end
