class ListVms

  # TODO: this is butt ugly
  # think about creating a mapping per so the vm attribute interface is standardised
  # also all this looping is horrid, perhaps it needs to be handed off to multiple tasks
  # or maybe the provider should format it to the way we want anyway
  # i see no good reason for the provider to return the raw data. they could return
  # an object of type ProviderServer (or something) with its attributes populated
  # which is then our known interface
  def self.perform(user, provider)
    # provider loop? or pass it in?
    user_provider = UserProvider.where(user: user, provider: provider).first

    provider = BuildVmConnection.perform(user_provider.provider, user_provider.api_key, user_provider.api_secret)

    projects = provider.list_projects
    project_vms = []
    projects['project'].each do |project|
      project_vms << provider.list_vms({'projectid' => project['id']})
    end
    vms = []
    project_vms.each do |virtual_machines|
      virtual_machines['virtualmachine'].each do |vm|
        vms << {
          'name' => vm['displayname'],
          'uuid' => vm['id'],
          'api_key' => user_provider.api_key,
          'api_secret' => user_provider.api_secret,
          'provider' => user_provider.provider.id
        }
      end
    end
    vms
  end
end
