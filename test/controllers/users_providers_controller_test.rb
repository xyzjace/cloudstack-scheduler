require 'test_helper'

class UsersProvidersControllerTest < ActionController::TestCase
  setup do
    @users_provider = users_providers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:users_providers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create users_provider" do
    assert_difference('UsersProvider.count') do
      post :create, users_provider: { api_key: @users_provider.api_key, api_secret: @users_provider.api_secret, provider_id: @users_provider.provider_id, user_id: @users_provider.user_id }
    end

    assert_redirected_to users_provider_path(assigns(:users_provider))
  end

  test "should show users_provider" do
    get :show, id: @users_provider
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @users_provider
    assert_response :success
  end

  test "should update users_provider" do
    patch :update, id: @users_provider, users_provider: { api_key: @users_provider.api_key, api_secret: @users_provider.api_secret, provider_id: @users_provider.provider_id, user_id: @users_provider.user_id }
    assert_redirected_to users_provider_path(assigns(:users_provider))
  end

  test "should destroy users_provider" do
    assert_difference('UsersProvider.count', -1) do
      delete :destroy, id: @users_provider
    end

    assert_redirected_to users_providers_path
  end
end
