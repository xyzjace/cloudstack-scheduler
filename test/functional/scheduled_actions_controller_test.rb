require 'test_helper'

class ScheduledActionsControllerTest < ActionController::TestCase
  setup do
    @scheduled_action = scheduled_actions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:scheduled_actions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create scheduled_action" do
    assert_difference('ScheduledAction.count') do
      post :create, scheduled_action: { scheduled_time: @scheduled_action.scheduled_time }
    end

    assert_redirected_to scheduled_action_path(assigns(:scheduled_action))
  end

  test "should show scheduled_action" do
    get :show, id: @scheduled_action
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @scheduled_action
    assert_response :success
  end

  test "should update scheduled_action" do
    put :update, id: @scheduled_action, scheduled_action: { scheduled_time: @scheduled_action.scheduled_time }
    assert_redirected_to scheduled_action_path(assigns(:scheduled_action))
  end

  test "should destroy scheduled_action" do
    assert_difference('ScheduledAction.count', -1) do
      delete :destroy, id: @scheduled_action
    end

    assert_redirected_to scheduled_actions_path
  end
end
