require 'test_helper'

class ActionResultsControllerTest < ActionController::TestCase
  setup do
    @action_result = action_results(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:action_results)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create action_result" do
    assert_difference('ActionResult.count') do
      post :create, action_result: { name: @action_result.name }
    end

    assert_redirected_to action_result_path(assigns(:action_result))
  end

  test "should show action_result" do
    get :show, id: @action_result
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @action_result
    assert_response :success
  end

  test "should update action_result" do
    put :update, id: @action_result, action_result: { name: @action_result.name }
    assert_redirected_to action_result_path(assigns(:action_result))
  end

  test "should destroy action_result" do
    assert_difference('ActionResult.count', -1) do
      delete :destroy, id: @action_result
    end

    assert_redirected_to action_results_path
  end
end
